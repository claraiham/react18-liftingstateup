import PropTypes from "prop-types";

function Profile(props) {
  const { user, setUser } = props;

  function login() {
    setUser({
      name: "Brandon",
    });
  }

  function logout() {
    setUser(null);
  }

  return (
    <div className="Profile">
      <h1>My profile</h1>
      {user ? (
        <div>
          <h3>Welcome {user?.name}!</h3>
          <button type="button" onClick={logout}>
            Logout
          </button>
        </div>
      ) : (
        <div>
          <input type="text" value="Brandon" disabled />
          <br />
          <input type="password" value="*************" disabled />
          <br />
          <button type="button" onClick={login}>
            Login
          </button>
        </div>
      )}
    </div>
  );
}

Profile.propTypes = {
  user: PropTypes.object,
  setUser: PropTypes.func.isRequired,
};

export default Profile;
