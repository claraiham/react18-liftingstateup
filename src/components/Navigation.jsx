import "./Navigation.css";

function Navigation({user}) { //J'indique que la fonction navigation prendra un paramètre nommé user
  // TODO get user from props drilling

  return (
    <ul className="Navigation">
      <li>
        <img src="https://via.placeholder.com/200x100" alt="Logo" />
      </li>
      <li>
        <h2>Awesome website!</h2>
      </li>
      {/* TODO check if user is connected */}
      <li>{user ? user.name : "Please log in"}</li> {/* Condition qui affiche le nom de l'utilisateur ou "log in" */}
    </ul>
  );
}

export default Navigation;
