import { useState } from "react";
import "./App.css";
import Navigation from "./components/Navigation";
import Profile from "./components/Profile";

function App() {
  const [user, setUser] = useState(null);

  return (
    <div className="App">
{/* Je donne la valeur du state au paramètre "user" défini dans Navigation */}
      <Navigation user= {user}/>
      <Profile user={user} setUser={setUser} />
    </div>
  );
}

export default App;
